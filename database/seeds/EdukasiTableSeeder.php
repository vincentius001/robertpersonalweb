<?php

use Illuminate\Database\Seeder;

class EdukasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		
		DB::table('edukasis')->truncate();
		DB::table('edukasis')->insert([
			['Title'=>"UNIVERSITAS GUNADARMA",
			'Tahun'=>"2016-NOW",
			'GPA'=>"-"],
			['Title'=>"SMA PSKD 7 DEPOK",
			'Tahun'=>"2013-2016",
			'GPA'=>"42"],
			['Title'=>"SMP IGNATIUS SLAMET RIYADI",
			'Tahun'=>"2010-2013",
			'GPA'=>"34"],
			['Title'=>"SDN CIBUBUR 8",
			'Tahun'=>"2010-2013",
			'GPA'=>"18"]
			]);
    }
}
