<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyBlogUser extends Model
{
    protected $table = 'user';
	public $timestamps = 'false';
	protected $primarykey = 'username';
}
