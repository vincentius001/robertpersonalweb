<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Edukasi;

class HomeController extends Controller
{
    //
	public function index()
	{
		$edukasi = Edukasi::all();
		return  view("MainPage", compact('edukasi'));
	}
}
