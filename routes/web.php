<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'HomeController@index'
]);

Route::get('/login', function () {
    return view('/user/Login');
});

Route::get('/daftar', function () {
    return view('/user/Create');
});


Route::resource('user', 'MyBlogController');