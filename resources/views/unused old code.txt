<div class="container">
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-4  container ctm-border2 con-oly">
				<img src="{{ asset('images/asisten.png') }}" alt="java" class="icn-expertise-setting">
					<div class="overlay">
						<div class="text-c bolder-text">LEPKOM</div>
						<div class="container">
							<div class="container">
								<p class="text-c">
									TUTOR ASISTANT<br>2018 - Now<br>
								</p>
								<p class="text-j">
									Helping practicioner on debuging code, syntax, asistant is like tutor side kick.
								</p>
							</div>
						</div>
					</div>
				</img>
			</div>
			<div class="col-sm-2"></div>
			<div class="col-sm-4  container ctm-border2 con-oly">
				<img src="{{ asset('images/asisten.png') }}" alt="java" class="icn-expertise-setting">
					<div class="overlay">
						<div class="text-c bolder-text">LEPKOM</div>
						<div class="container">
							<div class="container">
								<p class="text-c">
									TUTOR<br>2018 - Now<br>
								</p>
								<p class="text-j">
									Tutor is like professor asistant, Explain the theory then practice it, and the lesson is around
									computer scientist like making desktop programming, network, database, web programming, and many more.
								</p>
							</div>
						</div>
					</div>
				</img>
			</div>
			<div class="col-sm-1"></div>
		</div>
	</div>