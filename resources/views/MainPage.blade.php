<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<title>-- Robert Personal Web --</title>
		<link href="{{ asset('css/styles.css') }}" rel="stylesheet"></link>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"></link>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap-theme.min.css"></link>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</head>

	<section id="section-navbar" class="fixed-top">
		<div class="pos-f-t">
			<nav class="navbar navbar-dark bg-dark align-l-">
				<a href="#">
					<img src="{{ asset('images/home.png') }}" class="image-100"></img>
				</a>
				<a href="#about-s">
					<img src="{{ asset('images/about.png') }}" class="image-100"></img>
				</a>
				<a href="#education-s">
					<img src="{{ asset('images/education2.png') }}" class="image-100"></img>
				</a>
				<a href="#expertise-s">
					<img src="{{ asset('images/expertise.png') }}" class="image-105"></img>
				</a>
				<a href="#experience-s">
					<img src="{{ asset('images/experience.png') }}" class="image-100"></img>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
		</nav>
		<div class="collapse" id="navbarToggleExternalContent">
			<div class="bg-dark p-4">
				<h4 class="text-white">Admin Only Feature</h4>
				<a class="nav-link" href="{{ url('/login') }}">link</a>
			</div>
		</div>
	</div>
</section>

<!-- Full Page Image Header with Vertically Centered Content -->
<section id="masterhead-s">
	<header class="masthead">
		<div class="container h-100">
			<div class="row h-100 align-items-center">
				<div class="col-12 text-center">
					<h1 class="font-weight-light header-text header-text">WELCOME</h1>
					<p class="lead header-text">Want To Know About The Web Owner</p>
					<p class="lead header-text">Just Scroll Down!</p>
				</div>
			</div>
		</div>
	</header>
</header>

<!-- Page Content -->
<section class="py-5 html-bg-dark" id="about-s">
	<div class="lineborder-invert">
	</div>
	<div class="row title-about">
		<div class="col lineborder">
			<p>A B O U T</p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3">
			<div align="right">
				<img class="avatar-img" src="{{asset('images/avatar.png')}}" alt="My Avatar"></img>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="container bolder-text">
				<h2 class="font-weight-light">Robert</h2>
				<p class="font-weight-light">The Computer Sciencetist</p>
				<h2 class="font-weight-light">Quick Info</h2>
				<p class="font-weight-light">Hello i am programmer both desktop and website, i also study about general database, 
					im quite experienced in Java, but i currently study laravel more (this one using laravel)</p>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="container bolder-text">
				<p class="font-weight-light">about database i using mysql, phpmyadmin or sqlserver
					for database editor (to make my life easyer on handling database), and also i very love playing games, 
					By the way i love dark theme</p>
			</div>
		</div>
		<div class="col-sm-1">
		</div>
	</div>

</section>

<!-- SECTION EDUCATION -->
<section class="py-5 html-bg-dark" id="education-s">
	<div class="lineborder-invert">
	</div>
	<div class="row title-about">
		<div class="col lineborder">
			<p>E D U C A T I O N</p>
		</div>
	</div>
	@foreach($edukasi as $edukasi)
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-1 edu-icn"></div>
		<div class="col-sm-1 arw-icn"></div>
		<div class="col-sm-7">
			<h1 class="font-weight-light">
				{{$edukasi->Title}}
			</h1>
			<h3 class="">
			{{$edukasi->Tahun}}, GPA : {{$edukasi->GPA}}
			</h3>
		</div>
	</div>
	<br><br></br></br>
	@endforeach
		
</section>

<!-- SECTION EXPERTISE -->
<section class="py-5 html-bg-dark" id="expertise-s">
	<div class="lineborder-invert">
	</div>
	<div class="row title-about">
		<div class="col lineborder">
			<p>E X P E R T I S E</p>
		</div>
	</div>
	<div class="container expertiselist-padding">
		<div class="row">
			<div class="col-sm-3 container ctm-border con-oly">
				<img src="{{ asset('images/Java.png') }}" alt="java" class="icn-expertise-setting">
					<div class="overlay">
						<div class="text-c bolder-text">JAVA</div>
						<div class="container">
							<div class="container">
								<p class="text-j">
									The First Programming Langguage i learnt ever, Mastered General Knowegde of java, about 
									method and how to create them, and known about GUI concept like swing, awt, and also JApplet.
								</p>
							</div>
						</div>
					</div>
				</img>	
			</div>
			<div class="col-sm-1"></div>
			<div class="col-sm-3 container ctm-border con-oly">
				<img src="{{ asset('images/python.png') }}" alt="java" class="icn-expertise-setting">
					<div class="overlay">
						<div class="text-c bolder-text">PYTHON</div>
						<div class="container">
							<div class="container">
								<p class="text-j">
									The First Programming Langguage i learnt ever, known about general concept of looping 
									like : for, do while, branching like if else, switch, concept on how calling method
									and how to create them.
								</p>
							</div>
						</div>
					</div>
				</img>	
			</div>
			<div class="col-sm-1"></div>
			<div class="col-sm-3 container ctm-border con-oly">
				<img src="{{ asset('images/cshrap.png') }}" alt="java" class="icn-expertise-setting">
					<div class="overlay">
						<div class="text-c bolder-text">C#</div>
						<div class="container">
							<div class="container">
								<p class="text-j">
									The Third Programming langguage i learnt, in C# i mainly focusing on making web using MVC Web App,
									with razor view engine, not very expert but know general on how to make it
								</p>
							</div>
						</div>
					</div>
				</img>	
			</div>
		</div>
	</div>
	<div class="container expertiselist-padding">
		<div class="row">
			<div class="col-sm-3 container ctm-border con-oly">
				<img src="{{ asset('images/html.png') }}" alt="java" class="icn-expertise-setting">
					<div class="overlay">
						<div class="text-c bolder-text">HTML</div>
						<div class="container">
							<div class="container">
								<p class="text-j">
									The Fourth Programming Langguage... wait in a minute its not an a programming langguage anymore, its
									markup langguage, one of my experties and this is currently i studying, also this is my first completed
									website
								</p>
							</div>
						</div>
					</div>
				</img>	
			</div>
			<div class="col-sm-1"></div>
			<div class="col-sm-3 container ctm-border con-oly">
				<img src="{{ asset('images/laravel.png') }}" alt="java" class="icn-expertise-setting">
					<div class="overlay">
						<div class="text-c bolder-text">LARAVEL</div>
						<div class="container">
							<div class="container">
								<p class="text-j">
									I also study about laravel for developing web, I also currently learnt this framework since many people
									need this one, and this website using laravel framework, im still new so sorry if the website ugly :D
								</p>
							</div>
						</div>
					</div>
				</img>	
			</div>
			<div class="col-sm-1"></div>
			<div class="col-sm-3 container ctm-border con-oly">
				<img src="{{ asset('images/gaming.png') }}" alt="java" class="icn-expertise-setting">
					<div class="overlay">
						<div class="text-c bolder-text">GAMING</div>
						<div class="container">
							<div class="container">
								<p class="text-j">
									Im also loves at playing games, and expert, sometimes i love seeking bug in games, and testing on mechanic
									of the game, like the equipment effect, and many more, mainly i play shooting, survival, moba, rpg games.
								</p>
							</div>
						</div>
					</div>
				</img>	
			</div>
		</div>
	</div>
</section>

<!-- SECTION EXPERIENCE -->
<section class="py-5 html-bg-dark" id="experience-s">
	<div class="lineborder-invert">
	</div>
	<div class="row title-about">
		<div class="col lineborder">
			<p>E X P E R I E N C E</p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-1 ofc-icn"></div>
		<div class="col-sm-1 arw-icn"></div>
		<div class="col-sm-7">
			<h1 class="font-weight-light">
				LEPKOM
			</h1>
			<h3 class="">
				Tutor Asistant, 2018 - Now
			</h3>
		</div>
	</div>
	<br><br></br></br>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-1 ofc-icn"></div>
		<div class="col-sm-1 arw-icn"></div>
		<div class="col-sm-7">
			<h1 class="font-weight-light">
				LEPKOM
			</h1>
			<h3 class="">
				Tutor, 2018 - Now
			</h3>
		</div>
	</div>
	<br><br></br></br>
</section>

<!-- Footer -->
<footer class="page-footer font-small special-color-dark pt-4 bg-dark">

	<!-- Footer Elements -->
	<div class="container">

		<!-- Social buttons -->
		<ul class="list-unstyled list-inline text-center">
			<li class="list-inline-item">
				<a class="btn-floating btn-fb mx-1">
					<i class="fab fa-facebook-f"> </i>
				</a>
			</li>
			<li class="list-inline-item">
				<a class="btn-floating btn-tw mx-1">
					<i class="fab fa-twitter"> </i>
				</a>
			</li>
			<li class="list-inline-item">
				<a class="btn-floating btn-gplus mx-1">
					<i class="fab fa-google-plus-g"> </i>
				</a>
			</li>
			<li class="list-inline-item">
				<a class="btn-floating btn-li mx-1">
					<i class="fab fa-linkedin-in"> </i>
				</a>
			</li>
			<li class="list-inline-item">
				<a class="btn-floating btn-dribbble mx-1">
					<i class="fab fa-dribbble"> </i>
				</a>
			</li>
		</ul>
		<!-- Social buttons -->

	</div>
	<!-- Footer Elements -->

	<!-- Copyright -->
	<div class="footer-copyright text-center py-3">© 2019 Copyright
	</div>
	<!-- Copyright -->

</footer>
<!-- Footer -->

</html>
